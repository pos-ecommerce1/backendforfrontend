import { HttpException, HttpStatus, Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';
import { PassportStrategy } from '@nestjs/passport';
import { Request } from 'express';
import { ExtractJwt, Strategy } from 'passport-jwt';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy, 'jwt') {
  constructor(
    private readonly configService: ConfigService,
    private readonly jwtService: JwtService,
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromExtractors([
        ExtractJwt.fromAuthHeaderAsBearerToken(),
      ]),
      ignoreExpiration: true,
      secretOrKey: configService.get('jwt.access_token_secret'),
      passReqToCallback: true,
    });
  }

  async validate(request: Request) {
    const log = new Logger('jwtToken');
    try {
      const accessToken = request?.headers?.authorization.split(' ')[1];
      const userData = await this.jwtService.verify(accessToken, {
        secret: this.configService.get('jwt.access_token_secret'),
      });
      log.debug('pass verify jwt token');

      return userData;
    } catch (err) {
      log.debug(err);
      throw new HttpException(err, HttpStatus.UNAUTHORIZED);
    }
  }
}
