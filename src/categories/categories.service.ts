import { Injectable } from '@nestjs/common';
import { Client, ClientGrpc, Transport } from '@nestjs/microservices';
import { join } from 'path';
import { firstValueFrom, from, Observable } from 'rxjs';
import { CategoriesInterface } from './categories.interface';

@Injectable()
export class CategoriesService {
  @Client({
    transport: Transport.GRPC,
    options: {
      package: 'masterdata',
      url: 'localhost:5000',
      protoPath: join(__dirname, 'categories.proto'),
    },
  })
  client: ClientGrpc;

  private categoriesInterface: any;
  onModuleInit() {
    this.categoriesInterface =
      this.client.getService<CategoriesInterface>('CategoriesServices');
  }
  async getCategories(query: any) {
    const response = await firstValueFrom(
      this.categoriesInterface.getCategories('get-categories'),
    );
    const offset = query.perPage * (query.page - 1);
    const totalPages = Math.ceil(response['items'].length / query.perPage);
    const paginatedItems = response['items'].slice(
      offset,
      query.perPage * query.page,
    );
    if (offset || (totalPages && paginatedItems)) {
      return {
        previousPage: query.page - 1 ? query.page - 1 : null,
        currentpages: query.page,
        nextPage: totalPages > query.page ? query.page + 1 : null,
        total: response['items'].length,
        totalPages: totalPages,
        items: paginatedItems,
      };
    } else {
      return response['items'];
    }
  }
  createCategories(data: any): Observable<any> {
    return from(this.categoriesInterface.createCategories(data));
  }
  deleteCategories(id: any) {
    const data = { id: id };
    return this.categoriesInterface.deleteCategories(data);
  }
  updateCategories(id: any, body: any) {
    const data = { id: id, ...body };
    return this.categoriesInterface.updateCategories(data);
  }
}
