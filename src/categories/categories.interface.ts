import { Observable } from 'rxjs';

export interface CategoriesInterface {
  getCategories(data: InumberArray): Observable<any[]>;
  createCategories(data: DataCategories): Observable<any>;
  deleteCategories(data: DeleteCategories): Observable<any>;
}

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface InumberArray {}

export interface DataCategories {
  name: string;
}

export interface DeleteCategories {
  id: string;
}
