import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
} from '@nestjs/common';
import { from, Observable } from 'rxjs';
import { CategoriesService } from './categories.service';

@Controller('categories')
export class CategoriesController {
  constructor(private categoriesService: CategoriesService) {}
  @Get('/')
  async getCategories(@Query() query) {
    const roles = await this.categoriesService.getCategories(query);
    return roles;
  }
  @Post('/')
  createCategories(@Body() body: any): Observable<any> {
    return from(this.categoriesService.createCategories(body));
  }
  @Delete(':id')
  async deleteCategories(@Param('id') id: any) {
    return this.categoriesService.deleteCategories(id);
  }
  @Put(':id')
  async updateCategories(@Param('id') id: any, @Body() body: any) {
    return this.categoriesService.updateCategories(id, body);
  }
}
