import { Controller, Get, Param, Query } from '@nestjs/common';
import { DistrictService } from './district.service';

@Controller('district')
export class DistrictController {
  constructor(private districtService: DistrictService) {}
  @Get(':id')
  async getDistrict(@Query() query, @Param('id') id: number) {
    const roles = await this.districtService.getDistrict(query, id);
    return roles;
  }
}
