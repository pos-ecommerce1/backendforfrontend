import { Observable } from 'rxjs';

export interface DistrictInterface {
  getDistrict(data: InumberArray): Observable<any[]>;
}

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface InumberArray {}
