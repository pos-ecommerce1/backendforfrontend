import { Injectable } from '@nestjs/common';
import { Client, ClientGrpc, Transport } from '@nestjs/microservices';
import { join } from 'path';
import { firstValueFrom } from 'rxjs';
import { DistrictInterface } from './district.interface';

@Injectable()
export class DistrictService {
  @Client({
    transport: Transport.GRPC,
    options: {
      package: 'masterdata',
      url: 'localhost:5000',
      protoPath: join(__dirname, 'district.proto'),
    },
  })
  client: ClientGrpc;

  private districtInterface: any;
  onModuleInit() {
    this.districtInterface =
      this.client.getService<DistrictInterface>('DistrictServices');
  }
  async getDistrict(query: any, id: number) {
    const data = {
      name: query.name,
      id: id,
    };
    const response = await firstValueFrom(
      this.districtInterface.getDistrict(data),
    );
    return response;
  }
}
