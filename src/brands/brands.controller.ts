import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { from, Observable } from 'rxjs';
import { BrandsService } from './brands.service';
import { BrandDto } from './dto/brand.dto';

@Controller('brands')
export class BrandsController {
  constructor(private brandService: BrandsService) {}
  @UseGuards(AuthGuard('jwt'))
  @Get('/')
  async getBrand(@Query() query) {
    const roles = await this.brandService.getBrand(query);
    return roles;
  }
  @UseGuards(AuthGuard('jwt'))
  @Post('/')
  createBrand(@Body() body: BrandDto): Observable<any> {
    return from(this.brandService.createBrand(body));
  }
  @UseGuards(AuthGuard('jwt'))
  @Delete(':id')
  async deleteBrand(@Param('id') id: any) {
    return this.brandService.deleteBrand(id);
  }
  @UseGuards(AuthGuard('jwt'))
  @Put(':id')
  async updateBrand(@Param('id') id: any, @Body() body: BrandDto) {
    return this.brandService.updateBrand(id, body);
  }
}
