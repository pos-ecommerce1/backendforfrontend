import { Observable } from 'rxjs';

export interface BrandServices {
  getBrand(data: InumberArray): Observable<any[]>;
  createBrand(data: DataBrand): Observable<any>;
  deleteBrand(data: DeleteRole): Observable<any>;
  updateBrand(data: UpdateRole): Observable<any>;
}

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface InumberArray {}

export interface DataBrand {
  id: string;
  name: string;
  insertedAt: string;
  updatedAt: string;
}

export interface DeleteRole {
  id: string;
}
export interface UpdateRole {
  id: string;
  name: string;
  updatedAt: string;
}
