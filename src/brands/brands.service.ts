import { Injectable } from '@nestjs/common';
import { Client, ClientGrpc, Transport } from '@nestjs/microservices';
import { join } from 'path';
import { firstValueFrom, from, Observable } from 'rxjs';
import { BrandServices } from './brand.interface';

@Injectable()
export class BrandsService {
  @Client({
    transport: Transport.GRPC,
    options: {
      package: 'masterdata',
      url: 'localhost:5000',
      protoPath: join(__dirname, 'brand.proto'),
    },
  })
  client: ClientGrpc;

  private rolesinterface: any;
  onModuleInit() {
    this.rolesinterface =
      this.client.getService<BrandServices>('BrandServices');
  }
  async getBrand(query: any) {
    const offset = query.perPage * (query.page - 1);
    const items = await firstValueFrom(
      this.rolesinterface.getBrand({
        from: offset,
        total: query.perPage,
      }),
    );
    const totalPages = Math.ceil(items['total'].value / query.perPage);
    if (offset || totalPages) {
      return {
        previousPage: query.page - 1 ? query.page - 1 : null,
        currentpages: query.page,
        nextPage: totalPages > query.page ? query.page + 1 : null,
        total: items['total'].value,
        totalPages: totalPages,
        items: items['hits'],
      };
    } else {
      return { items: items['hits'] };
    }
  }
  createBrand(data: any): Observable<any> {
    return from(this.rolesinterface.createBrand(data));
  }
  deleteBrand(id: any) {
    const data = { id: id };
    return this.rolesinterface.deleteBrand(data);
  }
  updateBrand(id: any, body: any) {
    const data = { id: id, ...body };
    return this.rolesinterface.updateBrand(data);
  }
}
