import {
  Body,
  Controller,
  Post,
  UseGuards,
  Request,
  Headers,
  Get,
  Param,
} from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthGuard } from '@nestjs/passport';
@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}
  @Post('register')
  async register(@Body() body: any) {
    return this.authService.register(body);
  }
  @UseGuards(AuthGuard('local'))
  @Post('login')
  async login(@Request() req) {
    return this.authService.login(req.user, req);
  }
  @UseGuards(AuthGuard('jwt'))
  @Get('logout')
  async logout(@Request() req, @Headers() headers) {
    return this.authService.logout(req, headers);
  }
}
