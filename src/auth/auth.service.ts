import {
  Injectable,
  NotAcceptableException,
  UnauthorizedException,
} from '@nestjs/common';
import { Client, ClientGrpc, Transport } from '@nestjs/microservices';
import { join } from 'path';
import { firstValueFrom } from 'rxjs';
import { GRPCService } from './auth.interface';
import * as bcrypt from 'bcrypt';
import { JwtService } from '@nestjs/jwt';
import { ConfigService } from '@nestjs/config';
import * as moment from 'moment-timezone';
@Injectable()
export class AuthService {
  @Client({
    transport: Transport.GRPC,
    options: {
      package: 'account',
      url: 'localhost:5556',
      maxSendMessageLength: 10 * 1024 * 1024, // 10 MB
      maxReceiveMessageLength: 10 * 1024 * 1024, // 10 MB
      loader: { keepCase: true },
      protoPath: join(__dirname, 'user.proto'),
    },
  })
  client: ClientGrpc;

  private userInterface: any;
  onModuleInit() {
    this.userInterface = this.client.getService<GRPCService>('UserServices');
  }
  constructor(
    private jwtService: JwtService,
    private configService: ConfigService,
  ) {}
  //   async getRole() {
  //     const response = await firstValueFrom(
  //       this.rolesinterface.getRole('get-role'),
  //     );
  //     return response;
  //   }
  //   async getSingleRole(query: any) {
  //     console.log(query);
  //     const response = await firstValueFrom(
  //       this.rolesinterface.getSingleRole(query),
  //     );
  //     console.log(response);
  //     return response;
  //   }

  async register(data: any) {
    return await this.userInterface.createUser(data);
  }
  async validateUser(email: string, password: string): Promise<any> {
    const user = await firstValueFrom(
      this.userInterface.login({ email: email }),
    );
    if (!user['items']) return null;
    const passwordValid = await bcrypt.compare(
      password,
      user['items'][0]['_source']['password'],
    );
    if (!user) {
      throw new NotAcceptableException('could not find the user');
    }
    if (user['status'] === '0') {
      throw new UnauthorizedException('user tidak aktif');
    }
    const id = user['id'];
    // const member = await firstValueFrom(
    //   this.client.send('account-blog-get_single_member-userid', { id }),
    // );
    //const hasil = { ...user };
    if (user && passwordValid) {
      return user;
    }
    return null;
  }
  async login(user: any, req: any) {
    const data2 = { id: user['items'][0]['_source']['id'] };
    const data = {
      is_login: '1',
      ...data2,
    };
    await firstValueFrom(this.userInterface.updateStatusLogin(data));
    const payload = {
      email: user['items'][0]['_source']['email'],
      role: user['items'][0]['_source']['roles']['code'],
      name: user['items'][0]['_source']['profile'],
      sub: user['items'][0]['_id'],
    };
    // const dateNow = new Date();
    // const localDate = moment.tz(dateNow, 'Asia/Jakarta');
    // const ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    // const userAgent = req.headers['user-agent'];
    // const datalog = {
    //   ipaddress: ip,
    //   user_agent: userAgent,
    //   user: user.profile._source,
    //   action: 'Auth',
    //   messages: 'Melakukan Login ',
    //   createdAt: localDate,
    // };
    // await firstValueFrom(
    //   this.log.send('account-blog-create-log-user', datalog),
    // );
    return {
      id: user.id,
      role: user['items'][0]['_source']['roles']['code'],
      profile: user['items'][0]['_source']['profile']['fullname'],
      avatar: user['items'][0]['_source']['profile']['avatar'],
      access_token: this.jwtService.sign(payload, {
        secret: this.configService.get('jwt.access_token_secret'),
        expiresIn: `${this.configService.get('jwt.access_token_expiration')}`,
      }),
    };
  }

  async logout(req: any, headers: any) {
    const token = headers.authorization.split(' ')[1];
    const decodedJwtAccessToken = this.jwtService.decode(token);
    const datauser = JSON.parse(JSON.stringify(decodedJwtAccessToken));
    const data = { id: datauser.sub, is_login: '0' };
    return await firstValueFrom(this.userInterface.updateStatusLogin(data));
  }
  //   deleteRole(id: any) {
  //     const data = { id: id };
  //     return this.rolesinterface.deleteRole(data);
  //   }
  //   updateRole(id: any, body: any) {
  //     const data = { id: id, ...body };
  //     return this.rolesinterface.updateRole(data);
  //   }
}
