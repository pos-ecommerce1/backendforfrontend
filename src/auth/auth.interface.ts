import { Observable } from 'rxjs';

export interface GRPCService {
  getRole(data: InumberArray): Observable<any[]>;
  getSingleRole(data: QueryCode): Observable<any>;
  createUser(data: DataRole): Observable<any>;
  login(data: LoginData): Observable<any>;
  deleteRole(data: DeleteRole): Observable<any>;
}

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface InumberArray {}

export interface QueryCode {
  code: string;
}
export interface LoginData {
  email: string;
}
export interface DataRole {
  code: string;
  email: string;
  password: string;
}

export interface DeleteRole {
  id: string;
}
