import { Module } from '@nestjs/common';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { JwtService } from '@nestjs/jwt';
import { JwtStrategy } from 'src/strategy/jwt.strategy';
import { LocalStrategy } from 'src/strategy/local.strategy';
@Module({
  controllers: [AuthController],
  providers: [AuthService, JwtStrategy, LocalStrategy, JwtService],
})
export class AuthModule {}
