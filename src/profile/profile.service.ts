import { Injectable } from '@nestjs/common';
import { Client, ClientGrpc, Transport } from '@nestjs/microservices';
import { join } from 'path';
import { firstValueFrom, from, Observable } from 'rxjs';
import { ProfileInterface } from './profile.interface';

@Injectable()
export class ProfileService {
  @Client({
    transport: Transport.GRPC,
    options: {
      package: 'account',
      url: 'localhost:5556',
      maxSendMessageLength: 10 * 1024 * 1024, // 10 MB
      maxReceiveMessageLength: 10 * 1024 * 1024, // 10 MB
      loader: { keepCase: true },
      protoPath: join(__dirname, 'profile.proto'),
    },
  })
  client: ClientGrpc;

  private rolesinterface: any;
  onModuleInit() {
    this.rolesinterface =
      this.client.getService<ProfileInterface>('ProfileServices');
  }
  async getEmployee(query: any) {
    const offset = query.perPage * (query.page - 1);
    const items = await firstValueFrom(
      this.rolesinterface.getBrand({
        from: offset,
        total: query.perPage,
      }),
    );
    const totalPages = Math.ceil(items['total'].value / query.perPage);
    if (offset || totalPages) {
      return {
        previousPage: query.page - 1 ? query.page - 1 : null,
        currentpages: query.page,
        nextPage: totalPages > query.page ? query.page + 1 : null,
        total: items['total'].value,
        totalPages: totalPages,
        items: items['hits'],
      };
    } else {
      return { items: items['hits'] };
    }
  }
  createEmployee(data: any): Observable<any> {
    return from(this.rolesinterface.createBrand(data));
  }
  deleteEmployee(id: any) {
    const data = { id: id };
    return this.rolesinterface.deleteBrand(data);
  }
  updateEmployee(id: any, body: any) {
    const data = { id: id, ...body };
    return this.rolesinterface.updateBrand(data);
  }
}
