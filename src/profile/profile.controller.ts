import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { from, Observable } from 'rxjs';
import { ProfileService } from './profile.service';

@Controller('profile')
export class ProfileController {
  constructor(private brandService: ProfileService) {}
  @UseGuards(AuthGuard('jwt'))
  @Get('/')
  async getProfile(@Query() query) {
    const roles = await this.brandService.getEmployee(query);
    return roles;
  }
  @UseGuards(AuthGuard('jwt'))
  @Post('/')
  createBrand(@Body() body: any): Observable<any> {
    return from(this.brandService.createEmployee(body));
  }
  @UseGuards(AuthGuard('jwt'))
  @Delete(':id')
  async deleteBrand(@Param('id') id: any) {
    return this.brandService.deleteEmployee(id);
  }
  @UseGuards(AuthGuard('jwt'))
  @Put(':id')
  async updateBrand(@Param('id') id: any, @Body() body: any) {
    return this.brandService.updateEmployee(id, body);
  }
}
