import { Observable } from 'rxjs';

export interface CashierInterface {
  getCashier(data: InumberArray): Observable<any[]>;
  getSingleCashier(data: Params): Observable<any>;
  createCashier(data: DataUsers): Observable<any>;
  deleteCashier(data: DeleteUsers): Observable<any>;
}

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface InumberArray {}

export interface Params {
  id: string;
}

export interface DataUsers {
  fullname: string;
  avatar: string;
  gender: string;
  address: string;
  city: string;
  country: string;
  shop_id: string;
  postical_code: string;
}

export interface DeleteUsers {
  id: string;
}
