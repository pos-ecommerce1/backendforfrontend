import { Injectable } from '@nestjs/common';
import { Client, ClientGrpc, Transport } from '@nestjs/microservices';
import { join } from 'path';
import { firstValueFrom, from, Observable } from 'rxjs';
import { CashierInterface } from './cashier.interface';

@Injectable()
export class CashierService {
  @Client({
    transport: Transport.GRPC,
    options: {
      package: 'account',
      url: 'localhost:5556',
      maxSendMessageLength: 10 * 1024 * 1024, // 10 MB
      maxReceiveMessageLength: 10 * 1024 * 1024, // 10 MB
      loader: { keepCase: true },
      protoPath: join(__dirname, 'cashier.proto'),
    },
  })
  client: ClientGrpc;

  private userInterface: any;
  onModuleInit() {
    this.userInterface =
      this.client.getService<CashierInterface>('CashierServices');
  }
  async getCashier(query: any) {
    const offset = query.perPage * (query.page - 1);
    const items = await firstValueFrom(
      this.userInterface.getCashier({
        from: offset,
        total: query.perPage,
      }),
    );
    const totalPages = Math.ceil(items['total'].value / query.perPage);
    if (offset || totalPages) {
      return {
        previousPage: query.page - 1 ? query.page - 1 : null,
        currentpages: query.page,
        nextPage: totalPages > query.page ? query.page + 1 : null,
        total: items['total'].value,
        totalPages: totalPages,
        items: items['hits'],
      };
    } else {
      return { items: items['hits'] };
    }
  }
  async getSingleCashier(query: any) {
    const response = await firstValueFrom(
      this.userInterface.getSingleCashier(query),
    );
    return response;
  }
  createCashier(data: any): Observable<any> {
    return from(this.userInterface.createCashier(data));
  }
  deleteCashier(id: any) {
    const data = { id: id };
    return this.userInterface.deleteCashier(data);
  }
  updateCashier(id: any, body: any) {
    const data = { id: id, ...body };
    return this.userInterface.updateCashier(data);
  }
}
