import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { from, Observable } from 'rxjs';
import { CashierService } from './cashier.service';

@Controller('cashier')
export class CashierController {
  constructor(private cashierService: CashierService) {}
  @UseGuards(AuthGuard('jwt'))
  @Get('/')
  async getCashier(@Query() query) {
    const roles = await this.cashierService.getCashier(query);
    return { items: roles['items'] };
  }
  @UseGuards(AuthGuard('jwt'))
  @Get(':id')
  async getSingleCashier(@Param('id') id: number) {
    const roles = await this.cashierService.getSingleCashier(id);
    return roles;
  }
  @UseGuards(AuthGuard('jwt'))
  @Post('/')
  createCashier(@Body() body: any): Observable<any> {
    return from(this.cashierService.createCashier(body));
  }
  @UseGuards(AuthGuard('jwt'))
  @Delete(':id')
  async deleteCashier(@Param('id') id: any) {
    return this.cashierService.deleteCashier(id);
  }
  @UseGuards(AuthGuard('jwt'))
  @Put(':id')
  async updateCashier(@Param('id') id: any, @Body() body: any) {
    return this.cashierService.updateCashier(id, body);
  }
}
