import { Controller, Get, Param, Query } from '@nestjs/common';
import { RegencyService } from './regency.service';

@Controller('regency')
export class RegencyController {
  constructor(private regencyService: RegencyService) {}
  @Get(':id')
  async getRegency(@Query() query, @Param('id') id: number) {
    const roles = await this.regencyService.getRegency(query, id);
    return roles;
  }
}
