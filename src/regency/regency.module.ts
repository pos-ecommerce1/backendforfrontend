import { Module } from '@nestjs/common';
import { RegencyController } from './regency.controller';
import { RegencyService } from './regency.service';

@Module({
  controllers: [RegencyController],
  providers: [RegencyService],
})
export class RegencyModule {}
