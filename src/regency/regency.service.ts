import { Injectable } from '@nestjs/common';
import { Client, ClientGrpc, Transport } from '@nestjs/microservices';
import { join } from 'path';
import { firstValueFrom } from 'rxjs';
import { RegencyInterface } from './regency.interface';

@Injectable()
export class RegencyService {
  @Client({
    transport: Transport.GRPC,
    options: {
      package: 'masterdata',
      url: 'localhost:5000',
      protoPath: join(__dirname, 'regency.proto'),
    },
  })
  client: ClientGrpc;

  private regencyInterface: any;
  onModuleInit() {
    this.regencyInterface =
      this.client.getService<RegencyInterface>('RegencyServices');
  }
  async getRegency(query: any, id: number) {
    const data = {
      name: query.name,
      id: id,
    };
    const response = await firstValueFrom(
      this.regencyInterface.getRegency(data),
    );
    return response;
  }
}
