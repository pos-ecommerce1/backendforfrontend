import { Inject, Injectable, OnModuleInit } from '@nestjs/common';
import { Client, ClientGrpc, Transport } from '@nestjs/microservices';
import { join } from 'path';
import { firstValueFrom, from, Observable } from 'rxjs';
import { GRPCService } from './role.interface';
@Injectable()
export class RoleService implements OnModuleInit {
  @Client({
    transport: Transport.GRPC,
    options: {
      package: 'masterdata',
      protoPath: join(__dirname, 'role.proto'),
    },
  })
  client: ClientGrpc;

  private rolesinterface: any;
  onModuleInit() {
    this.rolesinterface = this.client.getService<GRPCService>('RoleServices');
  }
  async getRole() {
    const response = await firstValueFrom(
      this.rolesinterface.getRole('get-role'),
    );
    return response;
  }
  async getSingleRole(query: any) {
    const response = await firstValueFrom(
      this.rolesinterface.getSingleRole(query),
    );
    return response;
  }
  createRole(data: any): Observable<any> {
    return from(this.rolesinterface.createRole(data));
  }
  deleteRole(id: any) {
    const data = { id: id };
    return this.rolesinterface.deleteRole(data);
  }
  updateRole(id: any, body: any) {
    const data = { id: id, ...body };
    return this.rolesinterface.updateRole(data);
  }
}
