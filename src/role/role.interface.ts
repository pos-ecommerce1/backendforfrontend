import { Observable } from 'rxjs';

export interface GRPCService {
  getRole(data: InumberArray): Observable<any[]>;
  getSingleRole(data: QueryCode): Observable<any>;
  createRole(data: DataRole): Observable<any>;
  deleteRole(data: DeleteRole): Observable<any>;
}

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface InumberArray {}

export interface QueryCode {
  code: string;
}

export interface DataRole {
  code: string;
  name: string;
}

export interface DeleteRole {
  id: string;
}
