import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { from, Observable } from 'rxjs';
import { RoleDTO } from './dto/role.dto';
import { RoleService } from './role.service';

@Controller('role')
export class RoleController {
  constructor(private roleService: RoleService) {}
  @UseGuards(AuthGuard('jwt'))
  @Get('/')
  async getRole() {
    const roles = await this.roleService.getRole();
    return { items: roles['items'] };
  }
  @UseGuards(AuthGuard('jwt'))
  @Get('/get-role-by-code')
  async getSingleRole(@Query() query) {
    const roles = await this.roleService.getSingleRole(query);
    return roles;
  }
  @UseGuards(AuthGuard('jwt'))
  @Post('/')
  createRole(@Body() body: RoleDTO): Observable<any> {
    return from(this.roleService.createRole(body));
  }
  @UseGuards(AuthGuard('jwt'))
  @Delete(':id')
  async deleteRole(@Param('id') id: any) {
    return this.roleService.deleteRole(id);
  }
  @UseGuards(AuthGuard('jwt'))
  @Put(':id')
  async updateRole(@Param('id') id: any, @Body() body: RoleDTO) {
    return this.roleService.updateRole(id, body);
  }
}
