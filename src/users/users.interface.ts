import { Observable } from 'rxjs';

export interface UsersInterface {
  getUsers(data: InumberArray): Observable<any[]>;
  getSingleUsers(data: QueryCode): Observable<any>;
  createUsers(data: DataUsers): Observable<any>;
  deleteUsers(data: DeleteUsers): Observable<any>;
  assignEmployee(data: DeleteUsers): Observable<any>;
}

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface InumberArray {}

export interface QueryCode {
  code: string;
}

export interface DataUsers {
  code: string;
  name: string;
}

export interface DeleteUsers {
  id: string;
}
