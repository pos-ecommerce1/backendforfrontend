import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { Client, ClientGrpc, Transport } from '@nestjs/microservices';
import { query } from 'express';
import { join } from 'path';
import { firstValueFrom, from, Observable } from 'rxjs';
import { UsersInterface } from './users.interface';

@Injectable()
export class UsersService {
  @Client({
    transport: Transport.GRPC,
    options: {
      package: 'account',
      url: 'localhost:5556',
      maxSendMessageLength: 10 * 1024 * 1024, // 10 MB
      maxReceiveMessageLength: 10 * 1024 * 1024, // 10 MB
      loader: { keepCase: true },
      protoPath: join(__dirname, 'users.proto'),
    },
  })
  client: ClientGrpc;
  private userInterface: any;
  constructor(private jwtService: JwtService) {}
  onModuleInit() {
    this.userInterface = this.client.getService<UsersInterface>('UserServices');
  }
  async getSearchUser(query: any) {
    const items = await firstValueFrom(
      this.userInterface.getSearchUser({
        name: query.name,
      }),
    );
    return { items: items['hits'] };
  }
  async assignEmployeeDelete(id: number) {
    return await firstValueFrom(
      this.userInterface.assignEmployeeDelete({ id: id }),
    );
  }
  async getUsers(query: any) {
    const offset = query.perPage * (query.page - 1);
    const items = await firstValueFrom(
      this.userInterface.getUser({
        from: offset,
        total: query.perPage,
      }),
    );
    const totalPages = Math.ceil(items['total'].value / query.perPage);
    if (offset || totalPages) {
      return {
        previousPage: query.page - 1 ? query.page - 1 : null,
        currentpages: query.page,
        nextPage: totalPages > query.page ? query.page + 1 : null,
        total: items['total'].value,
        totalPages: totalPages,
        items: items['hits'],
      };
    } else {
      return { items: items['hits'] };
    }
  }
  async assignEmployee(body: any, headers: any) {
    const token = headers.authorization.split(' ')[1];
    const decodedJwtAccessToken = this.jwtService.decode(token);
    const datauser = JSON.parse(JSON.stringify(decodedJwtAccessToken));
    for (let index = 0; index < body.length; index++) {
      await firstValueFrom(
        this.userInterface.assignEmployee({
          shop_id: body[index].shop_id,
          user_id: body[index].user_id,
          owner_id: datauser.sub,
        }),
      );
    }
    return { messages: 'data sukses di assign' };
  }
  async assignEmployeeDetails(query: any, id: number, headers: any) {
    const token = headers.authorization.split(' ')[1];
    const decodedJwtAccessToken = this.jwtService.decode(token);
    const datauser = JSON.parse(JSON.stringify(decodedJwtAccessToken));
    const offset = query.perPage * (query.page - 1);
    const items = await firstValueFrom(
      this.userInterface.assignEmployeeDetails({
        from: offset,
        total: query.perPage,
        id: id,
      }),
    );
    const totalPages = Math.ceil(items['total'].value / query.perPage);
    if (offset || totalPages) {
      return {
        previousPage: query.page - 1 ? query.page - 1 : null,
        currentpages: query.page,
        nextPage: totalPages > query.page ? query.page + 1 : null,
        total: items['total'].value,
        totalPages: totalPages,
        items: items['hits'],
      };
    } else {
      return { items: items['hits'] };
    }
  }
  async assignEmployeeList(query: any, headers: any) {
    const token = headers.authorization.split(' ')[1];
    const decodedJwtAccessToken = this.jwtService.decode(token);
    const datauser = JSON.parse(JSON.stringify(decodedJwtAccessToken));
    const offset = query.perPage * (query.page - 1);
    const items = await firstValueFrom(
      this.userInterface.assignEmployeeList({
        owner_id: datauser.sub,
        from: offset,
        total: query.perPage,
      }),
    );
    const totalPages = Math.ceil(items['total'].value / query.perPage);
    if (offset || totalPages) {
      return {
        previousPage: query.page - 1 ? query.page - 1 : null,
        currentpages: query.page,
        nextPage: totalPages > query.page ? query.page + 1 : null,
        total: items['total'].value,
        totalPages: totalPages,
        items: items['hits'],
      };
    } else {
      return { items: items['hits'] };
    }
  }
  async getSingleUsers(query: any) {
    const response = await firstValueFrom(
      this.userInterface.getSingleRole(query),
    );
    return response;
  }
  createUsers(data: any): Observable<any> {
    return from(this.userInterface.createRole(data));
  }
  deleteUsers(id: any) {
    const data = { id: id };
    return this.userInterface.deleteRole(data);
  }
  updateUsers(id: any, body: any) {
    const data = { id: id, ...body };
    return this.userInterface.updateUser(data);
  }
}
