import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  Headers,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { query } from 'express';
import { from, Observable } from 'rxjs';
import { UsersService } from './users.service';

@Controller('users')
export class UsersController {
  constructor(private userService: UsersService) {}
  @UseGuards(AuthGuard('jwt'))
  @Get('/')
  async getUsers(@Query() query) {
    const roles = await this.userService.getUsers(query);
    return { items: roles['items'] };
  }
  @UseGuards(AuthGuard('jwt'))
  @Get('search')
  async searchUser(@Query() query) {
    const roles = await this.userService.getSearchUser(query);
    return roles;
  }
  @UseGuards(AuthGuard('jwt'))
  @Post('assign-employee')
  async assignEmployee(@Body() body: any, @Headers() headers: any) {
    const roles = await this.userService.assignEmployee(body, headers);
    return roles;
  }
  @UseGuards(AuthGuard('jwt'))
  @Get('assign-employee/list')
  async assignEmployeeList(@Query() query: any, @Headers() headers: any) {
    const roles = await this.userService.assignEmployeeList(query, headers);
    return roles;
  }
  @UseGuards(AuthGuard('jwt'))
  @Get('assign-employee/details/:id')
  async assignEmployeeDetails(
    @Param('id') id: number,
    @Query() query: any,
    @Headers() headers: any,
  ) {
    const roles = await this.userService.assignEmployeeDetails(
      query,
      id,
      headers,
    );
    return roles;
  }
  @UseGuards(AuthGuard('jwt'))
  @Delete('assign-employee/delete/:id')
  async assignEmployeeDelete(@Param('id') id: number) {
    const roles = await this.userService.assignEmployeeDelete(id);
    return roles;
  }
  @UseGuards(AuthGuard('jwt'))
  @Get(':id')
  async getSingleUsers(@Param('id') id: number) {
    const roles = await this.userService.getSingleUsers(id);
    return roles;
  }
  @UseGuards(AuthGuard('jwt'))
  @Post('/')
  createUsers(@Body() body: any): Observable<any> {
    return from(this.userService.createUsers(body));
  }
  @UseGuards(AuthGuard('jwt'))
  @Delete(':id')
  async deleteUsers(@Param('id') id: any) {
    return this.userService.deleteUsers(id);
  }
  @UseGuards(AuthGuard('jwt'))
  @Put(':id')
  async updateUsers(@Param('id') id: any, @Body() body: any) {
    return this.userService.updateUsers(id, body);
  }
}
