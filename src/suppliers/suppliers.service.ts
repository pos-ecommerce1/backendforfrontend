import { Injectable } from '@nestjs/common';
import { Client, ClientGrpc, Transport } from '@nestjs/microservices';
import { join } from 'path';
import { firstValueFrom, from, Observable } from 'rxjs';
import { SuppliersInterface } from './suppliers.interface';

@Injectable()
export class SuppliersService {
  @Client({
    transport: Transport.GRPC,
    options: {
      package: 'masterdata',
      url: 'localhost:5000',
      maxSendMessageLength: 10 * 1024 * 1024, // 10 MB
      maxReceiveMessageLength: 10 * 1024 * 1024, // 10 MB
      loader: { keepCase: true },
      // digunakan untuk mencovert data yang dari awalnya hexadecimal dari GRPC
      protoPath: join(__dirname, 'suppliers.proto'),
    },
  })
  client: ClientGrpc;

  private suppliersService: any;
  onModuleInit() {
    this.suppliersService =
      this.client.getService<SuppliersInterface>('SupplierServices');
  }
  async getSuppliers(query: any) {
    const offset = query.perPage * (query.page - 1);
    const items = await firstValueFrom(
      this.suppliersService.getSuppliers({
        from: offset,
        total: query.perPage,
      }),
    );
    const totalPages = Math.ceil(items['total'].value / query.perPage);
    if (offset || totalPages) {
      return {
        previousPage: query.page - 1 ? query.page - 1 : null,
        currentpages: query.page,
        nextPage: totalPages > query.page ? query.page + 1 : null,
        total: items['total'].value,
        totalPages: totalPages,
        items: items['hits'],
      };
    } else {
      return { items: items['hits'] };
    }
  }
  createSuppliers(data: any): Observable<any> {
    return from(this.suppliersService.createSuppliers(data));
  }
  deleteSuppliers(id: any) {
    const data = { id: id };
    return this.suppliersService.deleteSuppliers(data);
  }
  updateSuppliers(id: any, body: any) {
    const data = { id: id, ...body };
    return this.suppliersService.updateSuppliers(data);
  }
}
