import { Observable } from 'rxjs';

export interface SuppliersInterface {
  getSuppliers(data: InumberArray): Observable<any[]>;
  createSuppliers(data: DataSuppliers): Observable<any>;
  deleteSuppliers(data: DeleteSuppliers): Observable<any>;
  updateSuppliers(data: UpdateSuppliers): Observable<any>;
}

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface InumberArray {}

export interface DataSuppliers {
  id: string;
  name: string;
  description: string;
  tel_number: string;
  email: string;
  address: string;
  pic_name: string;
  pic_identity_number: string;
  pic_tel_number: string;
  villages_id: number;
  district_id: number;
  regency_id: number;
  province_id: number;
  insertedAt: string;
  updatedAt: string;
}

export interface DeleteSuppliers {
  id: string;
}
export interface UpdateSuppliers {
  id: string;
  name: string;
  description: string;
  tel_number: string;
  email: string;
  address: string;
  pic_name: string;
  pic_indentity_number: string;
  pic_tel_number: string;
  id_village: number;
  id_district: number;
  id_regency: number;
  id_province: number;
  updatedAt: string;
}
