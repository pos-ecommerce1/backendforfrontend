import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
} from '@nestjs/common';
import { from, Observable } from 'rxjs';
import { SupplierDTO } from './dto/supplier.dto';
import { SuppliersService } from './suppliers.service';

@Controller('suppliers')
export class SuppliersController {
  constructor(private supplierService: SuppliersService) {}
  @Get('/')
  async getSuppliers(@Query() query) {
    const roles = await this.supplierService.getSuppliers(query);
    return roles;
  }
  @Post('/')
  createSuppliers(@Body() body: SupplierDTO): Observable<any> {
    return from(this.supplierService.createSuppliers(body));
  }
  @Delete(':id')
  async deleteSuppliers(@Param('id') id: any) {
    return this.supplierService.deleteSuppliers(id);
  }
  @Put(':id')
  async updateSuppliers(@Param('id') id: any, @Body() body: SupplierDTO) {
    return this.supplierService.updateSuppliers(id, body);
  }
}
