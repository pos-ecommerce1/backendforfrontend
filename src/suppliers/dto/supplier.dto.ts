import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsOptional } from 'class-validator';

export class SupplierDTO {
  @ApiProperty()
  @IsNotEmpty()
  name: string;
  @ApiProperty()
  @IsOptional()
  description: string;
  @ApiProperty()
  @IsNotEmpty()
  tel_number: string;
  @ApiProperty()
  @IsNotEmpty()
  email: string;
  @ApiProperty()
  @IsOptional()
  address: string;
  @ApiProperty()
  @IsOptional()
  province_id: number;
  @ApiProperty()
  @IsOptional()
  regency_id: number;
  @ApiProperty()
  @IsOptional()
  district_id: string;
  @ApiProperty()
  @IsOptional()
  villages_id: string;
  @ApiProperty()
  @IsOptional()
  pic_name: string;
  @ApiProperty()
  @IsOptional()
  pic_identity_number: string;
  @ApiProperty()
  @IsOptional()
  pic_tel_number: number;
  @ApiProperty()
  @IsOptional()
  postical_code: number;
}
