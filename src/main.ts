import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { Logger, ValidationPipe } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { json } from 'body-parser';
import helmet from 'helmet';
import { WrapResponseInterceptor } from './common/interceptors/wrap-response.interceptor';
import { HttpExceptionFilter } from './common/interceptors/http-exception.filter';
import { envObjStart } from './config/envsub.config';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
async function bootstrap() {
  await envObjStart();
  const app = await NestFactory.create(AppModule);
  app.use(json({ limit: '50mb' }));

  const configService = app.get(ConfigService);

  // cors
  app.enableCors({ origin: true });
  const logger = new Logger('App');
  // security
  app.use(helmet());
  // app.use(compression());
  const PORT = configService.get('http.port') || 3000;
  app.useGlobalPipes(new ValidationPipe());
  app.useGlobalFilters(new HttpExceptionFilter());
  app.useGlobalInterceptors(new WrapResponseInterceptor());
  // initial for documentation API
  const config = new DocumentBuilder()
    .setTitle('Blog System Documentation')
    .setDescription('API docs backend for frontend vuejs.')
    .setVersion('1.0')
    .addBearerAuth()
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api', app, document);

  await app.listen(PORT);
  logger.log(`Application started on port ${PORT}`);
}
bootstrap();
