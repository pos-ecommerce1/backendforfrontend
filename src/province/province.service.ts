import { Injectable } from '@nestjs/common';
import { Client, ClientGrpc, Transport } from '@nestjs/microservices';
import { join } from 'path';
import { firstValueFrom } from 'rxjs';
import { ProvinceInterface } from './province.interface';

@Injectable()
export class ProvinceService {
  @Client({
    transport: Transport.GRPC,
    options: {
      package: 'masterdata',
      url: 'localhost:5000',
      protoPath: join(__dirname, 'province.proto'),
    },
  })
  client: ClientGrpc;

  private provinceInterface: any;
  onModuleInit() {
    this.provinceInterface =
      this.client.getService<ProvinceInterface>('ProvinceServices');
  }
  async getProvince(query: any) {
    const response = await firstValueFrom(
      this.provinceInterface.getProvince(query),
    );
    return response;
  }
}
