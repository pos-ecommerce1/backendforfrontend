import { Controller, Get, Query } from '@nestjs/common';
import { ProvinceService } from './province.service';

@Controller('province')
export class ProvinceController {
  constructor(private provinceService: ProvinceService) {}
  @Get('/')
  async getProvince(@Query() query) {
    const roles = await this.provinceService.getProvince(query);
    return roles;
  }
}
