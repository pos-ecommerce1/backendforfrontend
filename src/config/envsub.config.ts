import envsub from 'envsub';
import { join } from 'path';

const YAML_CONFIG_FILENAME = './config.yaml';
const {
  PORT,
  // jwt
  JWT_SECRET,
  JWT_EXP,
  JWT_EMAIL_SECRET,
  JWT_EMAIL_EXP,
  //service
} = process.env;

const templateFile = join(__dirname, YAML_CONFIG_FILENAME);
const outputFile = join(__dirname, YAML_CONFIG_FILENAME);

const options = {
  all: false,
  diff: false,
  envs: [
    // port
    { name: 'PORT', value: PORT },
    // jwt
    { name: 'JWT_SECRET', value: JWT_SECRET },
    { name: 'JWT_EXP', value: JWT_EXP },
    { name: 'JWT_EMAIL_SECRET', value: JWT_EMAIL_SECRET },
    { name: 'JWT_EMAIL_EXP', value: JWT_EMAIL_EXP },
  ],
  envFiles: [join(__dirname, YAML_CONFIG_FILENAME)],
  protect: false,
  syntax: 'default',
  system: true,
};

// create (or overwrite) the output file
export const envObjStart = () =>
  envsub({ templateFile, outputFile, options })
    .then(() => {
      console.log('env-sub has loaded');
    })
    .catch((err: Error) => {
      console.error(err.message);
    });
