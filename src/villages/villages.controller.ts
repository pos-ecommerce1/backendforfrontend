import { Controller, Get, Param, Query } from '@nestjs/common';
import { VillagesService } from './villages.service';

@Controller('villages')
export class VillagesController {
  constructor(private villagesService: VillagesService) {}
  @Get(':id')
  async getRegency(@Query() query, @Param('id') id: number) {
    const roles = await this.villagesService.getVillages(query, id);
    return roles;
  }
}
