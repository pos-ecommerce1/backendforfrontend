import { Injectable } from '@nestjs/common';
import { Client, ClientGrpc, Transport } from '@nestjs/microservices';
import { join } from 'path';
import { firstValueFrom } from 'rxjs';
import { VillagesInterface } from './villages.interface';

@Injectable()
export class VillagesService {
  @Client({
    transport: Transport.GRPC,
    options: {
      package: 'masterdata',
      url: 'localhost:5000',
      protoPath: join(__dirname, 'villages.proto'),
    },
  })
  client: ClientGrpc;

  private villagesInterface: any;
  onModuleInit() {
    this.villagesInterface =
      this.client.getService<VillagesInterface>('VillageServices');
  }
  async getVillages(query: any, id: number) {
    const data = {
      name: query.name,
      id: id,
    };
    const response = await firstValueFrom(
      this.villagesInterface.getVillages(data),
    );
    return response;
  }
}
