import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import configuration from './config/yaml.config';
import { ConfigModule } from '@nestjs/config';
import { RoleModule } from './role/role.module';
import { BrandsModule } from './brands/brands.module';
import { SuppliersModule } from './suppliers/suppliers.module';
import { CategoriesModule } from './categories/categories.module';
import { ProvinceModule } from './province/province.module';
import { RegencyModule } from './regency/regency.module';
import { DistrictModule } from './district/district.module';
import { VillagesModule } from './villages/villages.module';
import { AuthModule } from './auth/auth.module';
import { ProfileModule } from './profile/profile.module';
import { UsersModule } from './users/users.module';
import { CashierModule } from './cashier/cashier.module';
import { ShopModule } from './shop/shop.module';
@Module({
  imports: [
    ConfigModule.forRoot({ isGlobal: true, load: [configuration] }),
    RoleModule,
    BrandsModule,
    SuppliersModule,
    CategoriesModule,
    ProvinceModule,
    RegencyModule,
    DistrictModule,
    VillagesModule,
    AuthModule,
    ProfileModule,
    UsersModule,
    CashierModule,
    ShopModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
