import { Observable } from 'rxjs';

export interface ShopInterfaces {
  getShop(data: InumberArray): Observable<any[]>;
  createShop(data: DataShop): Observable<any>;
  deleteShop(data: DeleteShop): Observable<any>;
}

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface InumberArray {}

export interface DataShop {
  name: string;
  address: string;
  province_d: number;
  regency_id: number;
  villages_id: string;
  postical_code: string;
  district_id: string;
  user_id: number;
}

export interface DeleteShop {
  id: string;
}
