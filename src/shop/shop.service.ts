import { Injectable } from '@nestjs/common';
import { Client, ClientGrpc, Transport } from '@nestjs/microservices';
import { join } from 'path';
import { firstValueFrom, from, Observable } from 'rxjs';
import { ShopInterfaces } from './shop.interface';
import { JwtService } from '@nestjs/jwt';
@Injectable()
export class ShopService {
  @Client({
    transport: Transport.GRPC,
    options: {
      package: 'masterdata',
      url: 'localhost:5000',
      maxSendMessageLength: 10 * 1024 * 1024, // 10 MB
      maxReceiveMessageLength: 10 * 1024 * 1024, // 10 MB
      loader: { keepCase: true },
      protoPath: join(__dirname, 'shop.proto'),
    },
  })
  client: ClientGrpc;

  private shopInterface: any;
  constructor(private jwtService: JwtService) {}
  onModuleInit() {
    this.shopInterface = this.client.getService<ShopInterfaces>('ShopServices');
  }
  async getSingleShop(id: number) {
    return await firstValueFrom(this.shopInterface.getSingleShop({ id: id }));
  }
  async getShop(query: any, headers: any) {
    const token = headers.authorization.split(' ')[1];
    const decodedJwtAccessToken = this.jwtService.decode(token);
    const datauser = JSON.parse(JSON.stringify(decodedJwtAccessToken));
    const offset = query.perPage * (query.page - 1);
    const items = await firstValueFrom(
      this.shopInterface.getShop({
        from: offset,
        total: query.perPage,
        user_id: datauser.sub,
      }),
    );
    const totalPages = Math.ceil(items['total'].value / query.perPage);
    if (offset || totalPages) {
      return {
        previousPage: query.page - 1 ? query.page - 1 : null,
        currentpages: query.page,
        nextPage: totalPages > query.page ? query.page + 1 : null,
        total: items['total'].value,
        totalPages: totalPages,
        items: items['hits'],
      };
    } else {
      return { items: items['hits'] };
    }
  }
  createShop(data: any, headers: any): Observable<any> {
    const token = headers.authorization.split(' ')[1];
    const decodedJwtAccessToken = this.jwtService.decode(token);
    const datauser = JSON.parse(JSON.stringify(decodedJwtAccessToken));
    data['user_id'] = data.user_id ?? datauser.sub;
    return from(this.shopInterface.createShop(data));
  }
  deleteShop(id: any) {
    const data = { id: id };
    return this.shopInterface.deleteShop(data);
  }
  updateShop(id: any, body: any) {
    const data = { id: id, ...body };
    return this.shopInterface.updateShop(data);
  }
}
