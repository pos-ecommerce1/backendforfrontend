import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Headers,
  Post,
  Put,
  Query,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { from, Observable } from 'rxjs';
import { ShopService } from './shop.service';

@Controller('shop')
export class ShopController {
  constructor(private shopService: ShopService) {}
  @UseGuards(AuthGuard('jwt'))
  @Get('/')
  async getShop(@Query() query, @Headers() headers) {
    const roles = await this.shopService.getShop(query, headers);
    return roles;
  }
  @UseGuards(AuthGuard('jwt'))
  @Post('/')
  createShop(@Body() body: any, @Headers() headers): Observable<any> {
    return from(this.shopService.createShop(body, headers));
  }
  @UseGuards(AuthGuard('jwt'))
  @Delete(':id')
  async deleteShop(@Param('id') id: any) {
    return this.shopService.deleteShop(id);
  }
  @UseGuards(AuthGuard('jwt'))
  @Put(':id')
  async updateCategories(@Param('id') id: any, @Body() body: any) {
    return this.shopService.updateShop(id, body);
  }
  @UseGuards(AuthGuard('jwt'))
  @Get(':id')
  async getSingleShop(@Param('id') id: any) {
    return this.shopService.getSingleShop(id);
  }
}
